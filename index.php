<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Address Book</title>
  <link href="https://fonts.googleapis.com/css?family=Exo" rel="stylesheet">
  <link rel="stylesheet" href="main.css">
</head>
<body>
<div class="wrap">
  <h1 class="title">Address Book</h1>
  <table class="table-addressbook">
    <thead>
      <th>Photo</th>
      <th>Name</th>
      <th>Gender</th>
      <th>Location</th>
      <th>Email</th>
      <th>Phone</th>
    </thead>
    <tbody>
      <?php
        $source = 'https://randomuser.me/api/?results=5&format=json&nat=us&inc=name,gender,location,email,phone,picture';
        $results = json_decode(file_get_contents($source), true)['results'];
        $users = [];
        foreach ($results as $user) {
          $users[] = [
            'picture' => $user['picture']['large'],
            'name' => implode(' ', $user['name']),
            'gender' => $user['gender'],
            'location' => implode(', ', array_reverse(array_slice($user['location'], 0, 3))),
            'email' => $user['email'],
            'phone' => $user['phone']
          ];
        }

        foreach ($users as $user) :
      ?>
        <tr>
          <td><img class="user-photo" src="<?php echo $user['picture']?>" alt=""></td>
          <td class="user-name"><?php echo $user['name']?></td>
          <td class="user-gender"><?php echo $user['gender']?></td>
          <td class="user-location"><?php echo $user['location']?></td>
          <td class="user-email"><a href="mailto:<?php echo $user['email']?>"><?php echo $user['email']?></a></td>
          <td class="user-phone"><a href="tel:<?php echo $user['phone']?>"><?php echo $user['phone']?></a></td>
        </tr>
      <?php endforeach?>
    </tbody>
  </table>
</div>
</body>
</html>